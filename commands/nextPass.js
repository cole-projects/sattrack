const moment = require('moment-timezone');
const getFile = require('../lib/getFile');
const conf = require('../lib/config');
const sattrack = require('../lib/sattrack');
const scanRange = require('../lib/scanRange');

/**
 * The format string used to create a `Dusk` entry in the options array.
 * @type {String}
 */
const DUSKKEYFORMAT = 'MMM Do';
// formatting functions
var f2 = (f) => f.toFixed(2); // format float to 2

module.exports = function sample(prog) {
  var location = conf.get('location');
  prog
    .command('next', 'next [satellite]')
    .argument('<satellite>', 'Satellite or regex to match')
    .alias('n')
    .option(
      '--day [day]',
      'add or remove days to the reference date',
      prog.INT,
      0
    )
    .option(
      '--seconds [seconds]',
      'add or remove seconds to the reference date',
      prog.INT,
      0
    )
    .option('--range [range]', 'number of days to scan', prog.INT, 1)
    .option('--date [date]', 'set the reference date')
    .action(function (args, options, logger) {
      logger.debug('args', args);
      logger.debug('options', options);
      var argv = {};
      if (options.date)
        argv.now = moment(options.date, 'MM/DD/YYYY hh:mm:ss A');
      // console.log('NOW', argv);
      if (!argv.now) argv.now = moment();
      if (options.day) argv.now.add(argv.day, 'day');
      if (options.seconds) argv.now.add(argv.seconds, 'second');
      if (options.range)
        argv.enddate = argv.now.clone().add(options.range + options.day, 'day');

      // var tle = getFile(`./${options.tle}.txt`);

      argv.satellite = args.satellite;
      argv.tlefilename = `./${options.tle.replace(/\//, '-')}.txt`;

      // logger.debug("tle", tle);
      logger.debug('location', location);

      function check(argv, pos, options) {
        var t = moment(pos.time);
        var dusk = options.dusk[t.format(DUSKKEYFORMAT)];
        // console.log(t.format('LT MMM Do'), dusk.map(d => moment(d).format('LT MMM Do')), t.isBetween(dusk[0], dusk[1]));
        if (pos.sat.alt > 15 && t.isBetween(dusk[0], dusk[1])) {
          console.log(
            `"${moment(pos.time).format('LT MMM Do')}" ${f2(
              pos.sat.longitude
            )} ${f2(pos.sat.latitude)}`
          );
          return true;
        }
      }

      function done(argv, pass, options) {
        if (!pass)
          console.log(
            'no pass between',
            options.range.map((d) => moment(d).format('LT MMM Do'))
          );
      }

      scanRange(argv, {
        check,
        done,
      });
    });
};
