const moment = require('moment-timezone');
const getFile = require('../lib/getFile');
const conf = require('../lib/config');
const sattrack = require('../lib/sattrack');

module.exports = function sample(prog) {
  var location = conf.get('location');
  prog
    .command('visible', 'Visible tonight')
    .alias('v')
    .option(
      '--day [day]',
      'add or remove days to the reference date',
      prog.INT,
      0
    )
    .option(
      '--seconds [seconds]',
      'add or remove seconds to the reference date',
      prog.INT,
      0
    )
    .option('--range [range]', 'number of days to scan', prog.INT, 0)
    .option('--date [date]', 'set the reference date')
    .action(function (args, options, logger) {
      logger.debug('options', options);
      var argv = {};
      if (options.date)
        argv.now = moment(options.date, 'MM/DD/YYYY hh:mm:ss A');
      // console.log('NOW', argv);
      if (!argv.now) argv.now = moment();
      if (options.day) argv.now.add(argv.day, 'day');
      if (options.seconds) argv.now.add(argv.seconds, 'second');
      if (options.range)
        argv.enddate = argv.now.clone().add(options.range + options.day, 'day');

      var tle = getFile(`./${options.tle.replace(/\//, '-')}.txt`);

      // logger.debug("tle", tle);
      logger.debug('location', location);
      //   Rx.Observable.combineLatest(tle, getLocation(), function project(
      //     tle,
      //     location
      //   ) {
      //     return {
      //       tle,
      //       location
      //     };
      //   })
      var result = tle
        .map(function (data) {
          var options = {
            location,
            range: sattrack
              .duskRange(argv.now.toDate(), location)
              .map((d) => d.toDate()),
          };

          if (argv.immediate) options.range[0] = argv.now.toDate();

          return Object.assign(
            { tle: data },
            {
              pos: sattrack.visible(data, options),
            }
          );
        })
        .filter((x) => x.pos)
        //     .toArray()
        .sort((a, b) => (a.pos.time < b.pos.time ? -1 : 1))
        .forEach(function (data) {
          console.log(
            `${data.tle[0].trim()} visible at ${moment(data.pos.time).format(
              'LT'
            )} az:${data.pos.sat.az.toFixed(1)} alt:${data.pos.sat.alt.toFixed(
              1
            )}`
          );
        });
      //     .subscribe(function(positions) {
      //       var limit = p => (argv.limit ? p.slice(0, argv.limit) : p);
      //       // console.log(positions);
      //       limit(positions).forEach(function(data) {
      //         console.log(
      //           `${data.tle[0].trim()} visible at ${moment(data.pos.time).format(
      //             "LT"
      //           )} az:${data.pos.sat.az.toFixed(
      //             1
      //           )} alt:${data.pos.sat.alt.toFixed(1)}`
      //         );
      //       });
      //     });
    });
};
