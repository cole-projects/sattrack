// const util = require("util");
const fs = require('fs');
var moment = require('moment-timezone');
const httpGet = require('../lib/http');

/**
 * Scrape the nasa page for TLEs
 * @param  {String} body the body of the nasa response
 * @return {String}      a string of scraped TLEs
 */
function getNasa(body) {
  var search = [
    /Orbit ([^\)]*)/,
    /Vector Time \(GMT\): (.*)$/,
    /^....ISS$/,
    /.*/,
    /.*/,
  ];
  var sidx = 0;
  var tle = [];

  return body
    .split('\n')
    .reduce(function (file, line) {
      var match = search[sidx].exec(line);
      if (match) {
        tle.push(match.slice(-1)[0].trim());
        sidx++;
        if (sidx >= search.length) {
          // tle[1] Date format is 'YYYY/DDD/HH:mm:ss.SSS' 2019/338/15:42:20.087
          file.push(`${tle[2]} Orbit ${tle[0]} ${moment(
            tle[1],
            'YYYY/DDD/HH:mm:ss.SSS'
          ).format('LLL')}
${tle[3]}
${tle[4]}`);
          tle = [];
          sidx = 0;
        }
      }
      return file;
    }, [])
    .join('\n');
}

module.exports = function sample(prog) {
  prog
    .command('update', 'Update a tle file')
    .alias('u')
    .option(
      '--tle [tle]',
      'TLE dataset to use',
      [
        'visual',
        'tle-new',
        'stations',
        'weather',
        'supplemental/iss',
        'supplemental/starlink',
        'custom',
        'nasa',
      ],
      'visual'
    )
    .action(function (args, options, logger) {
      // args and options are objects
      // args = {"app": "myapp", "env": "production"}
      // options = {"tail" : 100}
      logger.info('options', options);
      //   logger.info("prog", util.inspect(prog, { depth: 1 }));
      //   logger.info("version", prog.version());

      const CELSTRACKURL = 'http://www.celestrak.com/NORAD/elements/';
      var tle_alias = {
        'supplemental/iss': {
          url: CELSTRACKURL + options.tle + '.txt',
          filename: 'supplemental-iss.txt',
        },
        'supplemental/starlink': {
          url: CELSTRACKURL + options.tle + '.txt',
          filename: 'supplemental-starlink.txt',
        },
        nasa: {
          url:
            'https://spaceflight.nasa.gov/realdata/sightings/SSapplications/Post/JavaSSOP/orbit/ISS/SVPOST.html',
          filename: 'nasa.txt',
          scraper: getNasa,
        },
      };
      var argv = {};
      if (tle_alias[options.tle]) {
        argv.tleurl = tle_alias[options.tle].url;
        argv.tlefilename = tle_alias[options.tle].filename;
        argv.scraper = tle_alias[options.tle].scraper;
      } else {
        argv.tleurl = CELSTRACKURL + options.tle + '.txt';
        argv.tlefilename = options.tle + '.txt';
      }

      return httpGet(argv.tleurl)
        .then(function (response) {
          if (argv.scraper) {
            response.body = argv.scraper(response.body);
          }

          fs.writeFileSync('./' + argv.tlefilename, response.body);
        })
        .catch(function (err) {
          logger.error(
            'error getting tle %s %s',
            argv.tlefilename,
            argv.tleurl,
            err
          );
        });
    });
};
