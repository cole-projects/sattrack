module.exports = function sample(prog) {
  prog
    .command("deploy", "Deploy an application")
    .argument("<app>", "App to deploy", /^myapp|their-app$/)
    .argument(
      "[env]",
      "Environment to deploy on",
      /^dev|staging|production$/,
      "local"
    )
    // you specify options using .option()
    // if --tail is passed, its value is required
    .option(
      "--tail <lines>",
      "Tail <lines> lines of logs after deploy",
      prog.INT
    )
    .action(function(args, options, logger) {
      // args and options are objects
      // args = {"app": "myapp", "env": "production"}
      // options = {"tail" : 100}
    });
};
