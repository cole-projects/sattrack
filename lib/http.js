const got = require("got");
const prog = require("caporal");
const repository = require("../package.json").repository;
module.exports = function HttpGet(url, options) {
  options = Object.assign(
    {},
    {
      headers: {
        "user-agent": `${prog.name()}/${prog.version()} (${repository.url})`
      }
    },
    options
  );
  //   console.log("HttpGet %j %j", url, options);
  return got.get(url, options);
};
