const fs = require("fs");
const chalk = require("chalk");
var moment = require("moment-timezone");

module.exports = function getFile(file) {
  if (!fs.existsSync(file)) {
    throw new Error(`TLE file '${file}' does not exist`);
  }
  var mtime = moment(fs.statSync(file).mtime);
  if (mtime.isBefore(moment().add(-2, "days"))) {
    console.log(
      chalk.yellow(
        `TLE file ${file} is ${mtime.fromNow(true)} old.  You should update it.`
      )
    );
  }

  return fs
    .readFileSync(file)
    .toString()
    .split("\n")
    .reduce(
      (tles, line, idx) => {
        if (line.length) {
          var currentIdx = tles.length - 1;
          var currentLength = tles[currentIdx].length;
          if (currentLength > 2) {
            tles.push([]);
            currentIdx = tles.length - 1;
            currentLength = tles[currentIdx].length;
          }
          tles[currentIdx].push(line.trimEnd());
        }
        return tles;
      },
      [[]]
    );
};
