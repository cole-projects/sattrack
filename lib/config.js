const Configstore = require("configstore");
const moment = require("moment-timezone");
const name = require("../package.json").name;

var conf = new Configstore(name, {
  tz: moment.tz.guess()
});

moment.tz.setDefault(conf.get("tz"));

module.exports = conf;
