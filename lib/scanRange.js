const conf = require("../lib/config");
const getFile = require("../lib/getFile");
var location = conf.get("location");
const sattrack = require("../lib/sattrack");

/**
 * The format string used to create a `Dusk` entry in the options array.
 * @type {String}
 */
const DUSKKEYFORMAT = "MMM Do";

/**
 * Returns a TLE array given a string match or
 * regular expression string.
 * @param  {String}   file      Tile file to search
 * @param  {String}   satellite Search string or regex if in `//`
 * @param  {Function} [cb]        Optional callback: `function(err, tle)`
 * @return {Array}            If no callback is passed in, an array is returned
 */
function getTLE(file, satellite, cb) {
  //   console.log("getTLE", file, satellite);
  // use a regular expression if the `satellite` string is
  // surrounded with `//`
  if (satellite.startsWith("/") && satellite.endsWith("/")) {
    var re = new RegExp(satellite.slice(1, -1), "i");
    var match = x => x.match(re);
  } else {
    var match = x => x.startsWith(satellite); // eslint-disable-line no-redeclare
  }

  var result = getFile(file).filter(function(x) {
    return match(x[0]);
  });
  //   .take(3)
  //     .reduce(function(result, value) {
  //       result.push(value);
  //       return result;
  //     }, []);

  if (cb) {
    //   observable.subscribe(function(tle) {
    //     cb(null, tle);
    //   }, cb);
    return cb(null, result || []);
  } else {
    //   return observable.toPromise(RSVP.Promise);
    return result || [];
  }
}

/**
 * can a range of times for a TLE and call a check function for each one.  If
 * the check function returns true, scanning stops.
 * @param  {Object}   argv       `yarg` arguments
 * @param  {Function}   getOptions function to create arguments with, with the format `function(tle, location)`
 * @param  {Function}   check      function to check position values with. `function(argv, pos)`
 * @param  {Function} done       [description]
 */
function withTleAndLocaton(argv, getOptions, check, done) {
  //   RSVP.hash({
  //     tle: getTLE(argv.tlefilename, argv.satellite),
  //     location: getLocation()
  //   })
  getTLE(argv.tlefilename, argv.satellite).map(function(results) {
    // console.log("withTleAndLocaton", results, argv);
    if (results.length == 0) {
      throw new Error(`no TLE found for satellite: "${argv.satellite}"`);
    }
    var options = Object.assign(
      {},
      {
        location
      },
      getOptions ? getOptions(results, location) : {}
    );
    // console.log("withTleAndLocaton", results, options);

    sattrack.range(results, options, function(pos) {
      return check(argv, pos);
    });

    if (done) return done(argv);
    else return argv;
  });
  //   .catch(function(err) {
  //   console.error("withTleAndLocaton Error:", err);
  // });
}

/**
 * Scans a range of times for visible positions.  Dusk ranges are
 * calculated for each day in the range and passed to the `hooks.check` function.
 * @param  {Object} argv  `yargs` arguments
 * @param  {Object} hooks object of hook functions.
 * @param  {Function} hooks.header called before the range loop starts. `function(tle, location)`
 * @param  {Function} hooks.check called on each range check.  If `check` returns true, the scan is stopped.  `function(argv, pos, options)`
 * @param  {Function} [hooks.done] called at the end of the range loop.  The last position that `check` returned true for is stored in `pass`.  `function(argv, pass)`
 */
module.exports = function scanRange(argv, hooks) {
  var pass,
    options = {},
    runs = 0;

  withTleAndLocaton(
    argv,
    function setOptions(tle, location) {
      options.location = location;
      //   console.log("withTleAndLocaton", argv);
      /*
      If the number of days in the range is 1, then the range and dusk values
      are just the dusk range, with one value in the dusk object.
     */
      var days = argv.enddate.diff(argv.now, "days");
      // console.log('days', days);
      options.dusk = {};
      if (days == 1) {
        options.range = sattrack
          .duskRange(argv.now.toDate(), location)
          .map(d => d.toDate());
        if (argv.immediate) options.range[0] = argv.now.toDate();
        // console.log('range', options);
        options.dusk[argv.now.format(DUSKKEYFORMAT)] = options.range;
      } else {
        /*
        It gets complex when scanning multiple days, the range start is the first
        days dusk start, and the range end is the last days dusk end.
        An entry for each day is needed in `dusk`.
       */
        var startDusk = sattrack
          .duskRange(argv.now.toDate(), location)[0]
          .toDate();
        var endDusk = sattrack
          .duskRange(argv.enddate.toDate(), location)[1]
          .toDate();
        options.range = [startDusk, endDusk];

        for (var i = 0; i <= days; i++) {
          var day = argv.now.clone().add(i, "days");
          options.dusk[day.format(DUSKKEYFORMAT)] = sattrack
            .duskRange(day.toDate(), location)
            .map(d => d.toDate());
        }
      }

      if (hooks.header) hooks.header(tle, location, options);

      return options;
    },
    function callCheck(argv, pos) {
      if (hooks.check(argv, pos, options)) {
        pass = pos;
        return true;
      }
    },
    function callDone() {
      // eslint-disable-line no-unused-vars
      if (hooks.done) hooks.done(argv, pass, options);
    }
  );
};
