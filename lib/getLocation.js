module.exports = function getLocation() {
  var location = conf.get("location");
  // console.log(chalk.green('getLocation %j'), location);

  if (location && location.latitude && location.longitude) {
    return RSVP.Promise.resolve(location);
  }

  return HttpGet(
    `http://api.ipstack.com?access_key=${process.env.IPSTACK_APIKEY}`,
    {
      json: true
    }
  ).then(function(location) {
    // console.log(chalk.red('getLocation %j'), location.body);
    conf.set("location", location.body);
    return location.body;
  });
};
