#!/usr/bin/env node

const pkg = require("./package.json");
const prog = require("caporal");
prog
  .version(pkg.version)
  .name(pkg.name)
  .description(pkg.description);

require("./commands/update")(prog);
require("./commands/visible")(prog);
require("./commands/nextPass")(prog);
require("./commands/plot")(prog);

/**
 * Add options from first commnd to all commands
 * as default.  @see https://github.com/mattallty/Caporal.js/issues/96
 */
const default_cmd = prog._commands[0];
for (const cmd of prog._commands.slice(1)) {
  cmd._options.unshift(...default_cmd._options);
}
prog.parse(process.argv);

// ./myprog deploy myapp production --tail 100
